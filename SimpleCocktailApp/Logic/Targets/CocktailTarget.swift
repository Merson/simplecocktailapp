//
//  CocktailTarget.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import Moya

public enum CocktailTarget {
    case search(String)
}

extension CocktailTarget: TargetType {
    public var baseURL: URL {
        return URL(string: "https://www.thecocktaildb.com/api/json")!
    }

    public var path: String {
        switch self {
        case .search:
            return "/v1/1/search.php"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .search:
            return .get
        }
    }

    public var sampleData: Data {
        return Data()
    }

    public var task: Task {
        switch self {
        case .search(let text):
            return .requestParameters(parameters: ["s": text], encoding: URLEncoding.default)
        }
    }

    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }

    public var validationType: ValidationType {
        return .successCodes
    }
}
