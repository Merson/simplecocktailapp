//
//  CocktailsApplicationLogic.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import Foundation
import Moya
import RxSwift

class CocktailsApplicationLogic {

    let provider = MoyaProvider<CocktailTarget>()

    public func fetchCocktails(text: String) -> Single<[Cocktail]> {
        return provider
            .rx
            .request(.search(text), callbackQueue: nil)
            .map(Array<LogicCocktail>.self, atKeyPath: "drinks", failsOnEmptyData: false)
            .map { logicCoctails -> [Cocktail] in
                return logicCoctails.map { $0.cocktail}
        }
    }
}
