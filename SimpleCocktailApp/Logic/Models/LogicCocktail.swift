//
//  LogicCocktail.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import Foundation
import Alamofire


/// A saveable and codable Tag entity. Conforme to Tags API.
internal final class LogicCocktail {
    var idDrink: String = ""
    var strDrink: String?
    var strDrinkAlternate: String?
    var strTags: String?
    var strIBA: String?
    var strCategory: String?
    var strAlcoholic: String?
    var strGlass: String?
    var strInstructions: String?
    var strDrinkThumb: String?
    var strIngredient1: String?
    var strIngredient2: String?
    var strIngredient3: String?
    var strIngredient4: String?
    var strIngredient5: String?
    var strIngredient6: String?
    var strIngredient7: String?
    var strIngredient8: String?
    var strIngredient9: String?
    var strIngredient10: String?
    var strIngredient11: String?
    var strIngredient12: String?
    var strIngredient13: String?
    var strIngredient14: String?
    var strIngredient15: String?
}

extension LogicCocktail: Decodable {
    enum CodingKeys: String, CodingKey {
        case idDrink
        case strDrink
        case strDrinkAlternate
        case strTags
        case strIBA
        case strCategory
        case strAlcoholic
        case strGlass
        case strInstructions
        case strDrinkThumb
        case strIngredient1
        case strIngredient2
        case strIngredient3
        case strIngredient4
        case strIngredient5
        case strIngredient6
        case strIngredient7
        case strIngredient8
        case strIngredient9
        case strIngredient10
        case strIngredient11
        case strIngredient12
        case strIngredient13
        case strIngredient14
        case strIngredient15
    }

    /*convenience init(from decoder: Decoder) throws {
        self.init()
        let values = try decoder.container(keyedBy: CodingKeys.self)

        idDrink = try values.decode(String.self, forKey: .idDrink)
        strDrink = try? values.decode(String.self, forKey: .strDrink)
        strDrinkAlternate = try? values.decode(String.self, forKey: .strDrinkAlternate)
        strTags = try? values.decode(String.self, forKey: .strTags)
        strIBA = try? values.decode(String.self, forKey: .strIBA)
        strCategory = try? values.decode(String.self, forKey: .strCategory)
        strAlcoholic = try? values.decode(String.self, forKey: .strAlcoholic)
        strGlass = try? values.decode(String.self, forKey: .strGlass)
        strInstructions = try? values.decode(String.self, forKey: .strInstructions)
        strDrinkThumb = try? values.decode(String.self, forKey: .strDrinkThumb)


        for i in 0...15 {
            let key = "\(CodingKeys.strIngredient.rawValue)\(i)"
            if let ingredient = try? values.decode(String.self, forKey: .strIngredient) {
                strIngredient.append(ingredient)
            }
        }
    }*/
}

extension LogicCocktail {
    var cocktail: Cocktail {
        let tmp = [
            strIngredient1,
            strIngredient2,
            strIngredient3,
            strIngredient4,
            strIngredient5,
            strIngredient6,
            strIngredient7,
            strIngredient8,
            strIngredient9,
            strIngredient10,
            strIngredient11,
            strIngredient12,
            strIngredient13,
            strIngredient14,
            strIngredient15,
        ]
        return Cocktail(
            idDrink: idDrink,
            strDrink: strDrink,
            strDrinkAlternate: strDrinkAlternate,
            strTags: strTags,
            strIBA: strIBA,
            strCategory: strCategory,
            strAlcoholic: strAlcoholic,
            strGlass: strGlass,
            strInstructions: strInstructions,
            imageUrl: URL(string: strDrinkThumb ?? ""),
            ingredients: tmp.compactMap { $0 })
    }
}
