//
//  ViewModel.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import Foundation
import RxSwift

class ViewModel {
    public let disposeBag = DisposeBag()

    init() {
        initRx()
    }

    internal func initRx() {
    }
}
