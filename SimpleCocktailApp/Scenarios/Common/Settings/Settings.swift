//
//  Settings.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import Foundation
import UIKit

class Settings {
    struct Display {
        static var defaultSearch: String = "a"
        static var margin: CGFloat = 5
        static var cell: CGFloat = 100
        static var debounceTime: Int = 500
        static var maxIngredients: Int = 2
    }
}
