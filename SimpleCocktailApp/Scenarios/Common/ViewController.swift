//
//  ViewController.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import UIKit
import RxSwift

class ViewController<VM: ViewModel>: UIViewController, Storyboarded {

    internal let disposeBag = DisposeBag()
    internal let viewModel: VM
    internal weak var coordinator: MainCoordinator?

    init(viewModel: VM) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        initRx()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUi()
        initConstraints()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    internal func initUi() {
        view.backgroundColor = .white
    }

    internal func initConstraints() {
    }

    internal func initRx() {
    }
}
