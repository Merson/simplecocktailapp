//
//  MainCoordinator.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import UIKit
import RxSwift

class MainCoordinator: Coordinator {

    let disposeBag = DisposeBag()

    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        startCocktails()
    }

    func startCocktails() {
        let vm = CocktailsViewModel()
        vm.cocktailDetailsSubject.withUnretained(self).bind { (me, cocktail) in
            me.startCocktailDetails(cocktail: cocktail)
        }.disposed(by: disposeBag)
        let vc = CocktailsViewController(viewModel: vm)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }

    func startCocktailDetails(cocktail: Cocktail) {
        let vm = CocktailDetailsViewModel(cocktail: cocktail)
        let vc = CocktailDetailsViewController(viewModel: vm)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
}
