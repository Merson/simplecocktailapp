//
//  CocktailDetailsViewModel.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import UIKit
import RxSwift
import RxSwiftExt
import RxCocoa

class CocktailDetailsViewModel: ViewModel {

    let cocktail: Cocktail

    init(cocktail: Cocktail) {
        self.cocktail = cocktail
        super.init()
    }

    override func initRx() {
        super.initRx()
    }
}
