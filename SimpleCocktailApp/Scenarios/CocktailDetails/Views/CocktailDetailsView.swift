//
//  CocktailDetailsView.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import UIKit
import SnapKit
import AlamofireImage

class CocktailDetailsView: UIView {

    private let stackView = UIStackView()
    private let cocktailImageView = UIImageView(image: UIImage(named: "placeholder"))
    private let strDrinkLabel = UILabel()
    private let strTagsLabel = UILabel()
    private let strIngredientsLabel = UILabel()
    private let strInstructionsLabel = UILabel()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(stackView)
        stackView.addArrangedSubview(strDrinkLabel)
        stackView.addArrangedSubview(strTagsLabel)
        stackView.addArrangedSubview(strIngredientsLabel)
        stackView.addArrangedSubview(strInstructionsLabel)
        stackView.addArrangedSubview(cocktailImageView)

        backgroundColor = .lightGray

        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = Settings.Display.margin * 2

        cocktailImageView.contentMode = .scaleAspectFit

        strDrinkLabel.numberOfLines = 0
        strDrinkLabel.textAlignment = .center
        strDrinkLabel.font = UIFont.boldSystemFont(ofSize: 40)

        strTagsLabel.numberOfLines = 0
        strIngredientsLabel.numberOfLines = 0
        strInstructionsLabel.numberOfLines = 0

    }

    private func initConstraints() {
        let margin = Settings.Display.margin
        stackView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(margin)
            make.left.equalToSuperview().offset(margin)
            make.right.equalToSuperview().offset(-margin)
            make.bottom.lessThanOrEqualToSuperview()
        }
    }

    public func fill(cocktail: Cocktail) {
        if let url = cocktail.imageUrl {
            DispatchQueue.main.async {
                self.cocktailImageView.af.setImage(withURL: url)
            }
        }
        strDrinkLabel.text = cocktail.strDrink
        strTagsLabel.text = cocktail.strDrinkAlternate
        var strIngredients: String = ""
        cocktail.ingredients.forEach { ingredient in
            strIngredients += "\(ingredient)  "
        }
        strIngredientsLabel.text = strIngredients
        strInstructionsLabel.text = cocktail.strInstructions
    }
}
