//
//  CocktailDetailsViewController.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import UIKit
import SnapKit
import RxSwift
import RxSwiftExt
import RxCocoa
import RxDataSources
import RxViewController
import RxGesture

class CocktailDetailsViewController: ViewController<CocktailDetailsViewModel> {

    private let scrollView = UIScrollView()
    private let cocktailDetailsView = CocktailDetailsView()

    override init(viewModel: CocktailDetailsViewModel) {
        super.init(viewModel: viewModel)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func initUi() {
        super.initUi()
        view.addSubview(scrollView)
        scrollView.addSubview(cocktailDetailsView)
        title = "Cocktail details"
        cocktailDetailsView.fill(cocktail: viewModel.cocktail)
    }

    override func initConstraints() {
        super.initConstraints()
        let guide = view.safeAreaLayoutGuide
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(guide.snp.top)
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(cocktailDetailsView.snp
                .height).priority(.medium)
        }

        cocktailDetailsView.snp.makeConstraints { make in
            make.top.equalTo(scrollView)
            make.bottom.equalTo(scrollView)
            make.left.equalTo(view)
            make.right.equalTo(view)
            make.width.equalTo(UIScreen.main.bounds.width)
        }
    }
}
