//
//  CocktailsViewModel.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import UIKit
import RxSwift
import RxSwiftExt
import RxCocoa

class CocktailsViewModel: ViewModel {

    public let textRelay: BehaviorRelay<String?>
    public let loadingRelay: PublishRelay<Bool>
    public let cocktailsRelay: PublishRelay<[Cocktail]>
    public let cocktailsSignal: Signal<[Cocktail]>
    public let cocktailDetailsSubject: PublishSubject<Cocktail>

    private let cocktailsApplicationLogic = CocktailsApplicationLogic()

    override init() {
        textRelay = BehaviorRelay<String?>(value: Settings.Display.defaultSearch)
        loadingRelay = PublishRelay<Bool>()
        cocktailsRelay = PublishRelay<[Cocktail]>()
        cocktailsSignal = cocktailsRelay.asSignal(onErrorJustReturn: [])
        cocktailDetailsSubject = PublishSubject<Cocktail>()
        super.init()
    }

    override func initRx() {
        super.initRx()

        let queue = MainScheduler.instance

        let textEvent = textRelay
            .debounce(.milliseconds(Settings.Display.debounceTime), scheduler: queue)
            .share()

        textEvent
            .observe(on: queue)
            .map { _ in true }
            .bind(to: loadingRelay)
            .disposed(by: disposeBag)

        textEvent
            .observe(on: queue)
            .withUnretained(self)
            .flatMap { $0.0.cocktailsApplicationLogic.fetchCocktails(text: $0.1 ?? Settings.Display.defaultSearch).catchAndReturn([]) }
            .bind(to: cocktailsRelay).disposed(by: disposeBag)

        cocktailsRelay
            .observe(on: queue)
            .withLatestFrom(loadingRelay)
            .filter { $0 }
            .map { _ in false }
            .bind(to: loadingRelay)
            .disposed(by: disposeBag)
    }
}
