//
//  CocktailsViewController.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import UIKit
import SnapKit
import RxSwift
import RxSwiftExt
import RxCocoa
import RxDataSources
import RxViewController
import NVActivityIndicatorView


class CocktailsViewController: ViewController<CocktailsViewModel> {

    private lazy var loader = NVActivityIndicatorView(frame: view.frame, type: .ballTrianglePath, color: .black, padding: 0)
    private let textField = UITextField()
    private let clearButton = UIButton()
    private let cocktailsTableView = UITableView(frame: .zero, style: .plain)

    override init(viewModel: CocktailsViewModel) {
        super.init(viewModel: viewModel)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func initUi() {
        super.initUi()
        title = "Cocktails"
        view.addSubview(loader)
        view.addSubview(textField)
        view.addSubview(clearButton)
        view.addSubview(cocktailsTableView)

        view.bringSubviewToFront(loader)

        textField.placeholder = "Type a cocktail ex: margarita"

        clearButton.setImage(UIImage(named: "clear"), for: .normal)

        cocktailsTableView.contentInset.top = Settings.Display.margin * 2
        cocktailsTableView.contentInset.bottom = -(Settings.Display.margin * 2)
        cocktailsTableView.backgroundColor = .white
        cocktailsTableView.allowsSelection = true
        cocktailsTableView.separatorStyle = .none
        cocktailsTableView.estimatedRowHeight = Settings.Display.cell
        cocktailsTableView.rowHeight = UITableView.automaticDimension
        cocktailsTableView.register(cellType: CocktailTableViewCell.self)
    }

    override func initConstraints() {
        super.initConstraints()
        let guide = view.safeAreaLayoutGuide
        let margin = Settings.Display.margin
        loader.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
        }
        textField.snp.makeConstraints { (make) in
            make.top.equalTo(guide.snp.top)
            make.left.equalToSuperview()
            make.right.equalTo(clearButton.snp.left)
            make.height.equalTo(40)
        }
        clearButton.snp.makeConstraints { (make) in
            make.top.equalTo(guide.snp.top).offset(margin)
            make.right.equalToSuperview().offset(-margin)
            make.width.equalTo(clearButton.snp.height)
            make.height.equalTo(30)
        }
        cocktailsTableView.snp.makeConstraints { (make) in
            make.top.equalTo(textField.snp.bottom)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }

    override func initRx() {
        super.initRx()
        cocktailsTableView.dataSource = nil

        let queue = MainScheduler.instance

        textField
            .rx
            .text
            .filter { $0 != "" }
            .startWith(Settings.Display.defaultSearch)
            .bind(to: viewModel.textRelay)
            .disposed(by: disposeBag)

        viewModel
            .cocktailsRelay
            .observe(on: queue)
            .bind(to: cocktailsTableView.rx.items(
                cellIdentifier: String(describing: CocktailTableViewCell.self),
                cellType: CocktailTableViewCell.self)) { row, cocktail, cell in
                    cell.fill(cocktail: cocktail)
                }.disposed(by: disposeBag)

        cocktailsTableView
            .rx
            .modelSelected(Cocktail.self)
            .bind(to: viewModel.cocktailDetailsSubject)
            .disposed(by: disposeBag)

        viewModel
            .loadingRelay
            .observe(on: queue)
            .withUnretained(self)
            .bind { me, isLoading in
                if isLoading {
                    me.loader.startAnimating()
                } else {
                    me.loader.stopAnimating()
                }
            }.disposed(by: disposeBag)

        clearButton
            .rx
            .tap
            .map { Settings.Display.defaultSearch }
            .bind(to: viewModel.textRelay)
            .disposed(by: disposeBag)

        clearButton
            .rx
            .tap
            .withUnretained(self)
            .bind { me, _ in
                me.textField.text = nil
            }.disposed(by: disposeBag)
    }
}
