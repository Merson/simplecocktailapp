//
//  CocktailTableViewCell.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import UIKit

class CocktailTableViewCell: TableViewCell {

    public let view = CocktailView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        clipsToBounds = false
        contentView.addSubview(view)
    }

    private func initConstraints() {
        let margin = Settings.Display.margin
        view.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(margin)
            make.right.equalToSuperview().offset(-margin)
            make.bottom.equalToSuperview().offset(-margin)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        view.clean()
    }

    public func fill(cocktail: Cocktail) {
        view.fill(cocktail: cocktail)
    }
}
