//
//  CocktailView.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import UIKit
import SnapKit
import AlamofireImage

class CocktailView: UIView {

    private let cocktailImageView = UIImageView(image: UIImage(named: "placeholder"))
    private let strDrinkLabel = UILabel()
    private let strTagsLabel = UILabel()
    private let strIngredientsLabel = UILabel()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(cocktailImageView)
        addSubview(strDrinkLabel)
        addSubview(strTagsLabel)
        addSubview(strIngredientsLabel)

        strDrinkLabel.numberOfLines = 0
        strDrinkLabel.textAlignment = .center
        strDrinkLabel.font = UIFont.boldSystemFont(ofSize: 20)

        strTagsLabel.numberOfLines = 0
        strIngredientsLabel.numberOfLines = 0

        backgroundColor = .lightGray
    }

    private func initConstraints() {
        let margin = Settings.Display.margin
        let cell = Settings.Display.cell
        snp.makeConstraints { make in
            make.height.equalTo(cell)
        }
        cocktailImageView.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview()
            make.width.equalTo(cocktailImageView.snp.height)
        }
        strDrinkLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalTo(cocktailImageView.snp.right).offset(margin)
            make.right.lessThanOrEqualToSuperview().offset(margin)
        }
        strTagsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(strDrinkLabel.snp.bottom).offset(margin)
            make.left.equalTo(cocktailImageView.snp.right).offset(margin)
            make.right.lessThanOrEqualToSuperview().offset(margin)
        }
        strIngredientsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(strTagsLabel.snp.bottom).offset(margin)
            make.left.equalTo(cocktailImageView.snp.right).offset(margin)
            make.right.lessThanOrEqualToSuperview().offset(margin)
        }
    }

    public func fill(cocktail: Cocktail) {
        if let url = cocktail.imageUrl {
            DispatchQueue.main.async {
                self.cocktailImageView.af.setImage(withURL: url)
            }
        }

        strDrinkLabel.text = cocktail.strDrink
        strTagsLabel.text = cocktail.strTags
        var strIngredients: String = ""
        cocktail.ingredients.prefix(Settings.Display.maxIngredients).forEach { ingredient in
            strIngredients += "\(ingredient)  "
        }
        strIngredientsLabel.text = strIngredients
    }

    public func clean() {
        cocktailImageView.image = nil
        strDrinkLabel.text = ""
        strTagsLabel.text = ""
        strIngredientsLabel.text = ""
    }
}
