//
//  CustomTextView.swift
//  SimpleCocktailApp
//
//  Created by Luc-Olivier MERSON on 26/10/2021.
//

import Foundation
import UIKit
import RxSwift

class CustomTextField: UITextField {

    private let clearButton = UIButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    public func fill(clearSubject: PublishSubject<Void>, disposeBag: DisposeBag) {
        clearButton
            .rx
            .tap
            .bind(to: clearSubject)
            .disposed(by: disposeBag)
        clearButton
            .rx
            .tap
            .withUnretained(self)
            .bind { me, _ in
                me.text = nil
            }.disposed(by: disposeBag)
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(clearButton)

        rightViewMode = .always

        clearButton.isUserInteractionEnabled = true
        clearButton.setImage(UIImage(named: "clear"), for: .normal)
    }

    private func initConstraints() {
        let margin = Settings.Display.margin
        clearButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(margin)
            make.right.equalToSuperview().offset(-margin)
            make.bottom.equalToSuperview().offset(-margin)
            make.width.equalTo(clearButton.snp.height)
        }
    }
}
