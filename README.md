# README #
Cette petite application permet de voir la liste des cocktails pour une recherche donnée. Par défault on recherche les cocktails qui commencent par la lettre "a".

J'utilise principalement RxSwift pour la programmation réactive, la pattern Coordinator + MVVM et du NoStoryboard.

### How do I get set up? ###
Installer cocoapods: sudo gem install cocoapods
Installer les pods nécéssaires: pod install